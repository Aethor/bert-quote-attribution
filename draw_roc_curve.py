import json
from copy import deepcopy

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from tqdm import tqdm

from config import (
    get_args,
    user_arg_definitions,
    arg_threshold,
    arg_sequential_threshold,
)
from datas import Dataset, Document
from static import get_tokenizer, get_device
from model import SpeakerPredictorModel
from predict import predict_speakers
from score import score_speakers


if __name__ == "__main__":

    user_arg_definitions = user_arg_definitions - set(
        [arg_threshold, arg_sequential_threshold]
    )
    args = get_args(user_arg_definitions)
    print(f"running with config : {json.dumps(vars(args), indent=4)}")

    tokenizer = get_tokenizer(args.bert_tokenizer_type)
    device = get_device(args.device)

    if len(args.input_files) == 0:
        print("no input files. exitting...")
        exit()

    socre_dataset = Dataset(
        Document.documents_from_descriptions(args.input_files, args.quote_context_size)
    )

    model = SpeakerPredictorModel.load(args.model_path)

    definition = 25
    thresholds = [t / definition for t in range(0, definition + 1)]
    raw_precisions = []
    raw_recalls = []
    raw_f1s = []

    tqdm.write("raw prediction...")
    score_dataset = predict_speakers(model, score_dataset, 0)

    tqdm.write("sequential predictions...")
    for threshold in tqdm(thresholds):

        for chapter in score_dataset.chapters:
            for quote in chapter.quotes:
                if quote.predicted_speaker is None:
                    continue
                if quote.predicted_speaker_scores[quote.predicted_speaker] < threshold:
                    quote.predicted_speaker = None

        precision, recall, f1 = score_speakers(score_dataset)
        raw_precisions.append(precision)
        raw_recalls.append(recall)
        raw_f1s.append(f1)

    plt.xlabel("recall")
    plt.ylabel("precision")
    plt.title("Precision versus Recall")
    legend_handles = [mpatches.Patch(color="blue", label="Raw model")]
    plt.legend(handles=legend_handles)
    plt.plot(raw_recalls, raw_precisions)

    plt.show()

import json
from typing import List, Optional, Tuple

from predict import predict_addressees, predict_speakers
from datas import Dataset, Document
from model import SpeakerPredictorModel
from config import get_args, user_arg_definitions
from static import get_tokenizer, get_device


def metrics_from_list(
    precision_list: List[int], recall_list: List[int]
) -> Tuple[Optional[float], Optional[float], Optional[float]]:
    """
    Computes precision, recall and F1 from precision and recall list
    
    :param precision_list: A list containing 1s for true positives and 0s for false positives
    :param recall_list: A list containing 1s for true negatives and 0s for false positives
    :return: | a 3-tuple
             | **precision** : Optional[float]
             | **recall** : Optional[float]
             | **F1** : Optional[float]
    """
    precision = (
        None if len(precision_list) == 0 else sum(precision_list) / len(precision_list)
    )
    recall = None if len(recall_list) == 0 else sum(recall_list) / len(recall_list)
    if not precision is None and not recall is None and precision != 0 and recall != 0:
        f1 = 2 * precision * recall / (precision + recall)
    else:
        f1 = None

    return precision, recall, f1


def score_speakers(
    dataset: Dataset,
) -> Tuple[Optional[float], Optional[float], Optional[float]]:
    """
    Computes speaker prediction metrics

    :param dataset: the input dataset, tagged with speaker prediction annotation
    :return: | a 3-tuple
             | **precision** : Optional[float]
             | **recall** : Optional[float]
             | **F1** : Optional[float]
    """
    precision_list = []
    recall_list = []

    for chapter in dataset.chapters:

        for quote in chapter.quotes:

            if quote.speaker is None:
                raise Exception("quote gold speaker is None")

            if quote.speaker == quote.predicted_speaker:
                precision_list.append(1)
                recall_list.append(1)
            elif not quote.predicted_speaker is None:
                precision_list.append(0)
            else:
                recall_list.append(0)

    return metrics_from_list(precision_list, recall_list)


def score_addressees_exact(dataset: Dataset) -> float:
    """
    Computes addressees prediction exact accuracy 

    :param dataset: the input dataset, tagged with addressees prediction annotation
    :return: exact accuracy
    """
    accuracy_list = []

    for chapter in dataset.chapters:

        for quote in chapter.quotes:

            if quote.addressees == quote.predicted_addressees:
                accuracy_list.append(1)
            else:
                accuracy_list.append(0)

    return None if len(accuracy_list) == 0 else sum(accuracy_list) / len(accuracy_list)


def score_addressees(dataset: Dataset):
    """
    Computes addressees prediction metrics

    :param dataset: the input dataset, tagged with addressees prediction annotation
    :return: | a 3-tuple
             | **precision** : Optional[float]
             | **recall** : Optional[float]
             | **F1** : Optional[float]
    """
    precision_list = []
    recall_list = []

    for chapter in dataset.chapters:

        for quote in chapter.quotes:

            for _ in quote.addressees.intersection(quote.predicted_addressees):
                precision_list.append(1)
                recall_list.append(1)
            for _ in quote.addressees.difference(quote.predicted_addressees):
                recall_list.append(0)
            for _ in quote.predicted_addressees.difference(quote.addressees):
                precision_list.append(0)

    return metrics_from_list(precision_list, recall_list)


if __name__ == "__main__":

    args = get_args(user_arg_definitions)

    print(f"running with config : {json.dumps(vars(args), indent=4)}")

    tokenizer = get_tokenizer(args.bert_tokenizer_type)
    device = get_device(args.device)

    if len(args.input_files) == 0:
        print("no input files. exitting...")
        exit()

    score_dataset = Dataset(
        Document.documents_from_descriptions(args.input_files, args.quote_context_size)
    )

    model = SpeakerPredictorModel.load(args.model_path)

    print("scoring speaker prediction")
    score_dataset = predict_speakers(model, score_dataset, args.threshold)
    precision, recall, f1 = score_speakers(score_dataset)
    print(f"raw precision : {precision}")
    print(f"raw recall : {recall}")
    print(f"raw f1-score : {f1}")

    print("scoring addressees prediction")
    score_dataset = predict_addressees(score_dataset)
    precision, recall, f1 = score_addressees(score_dataset)
    print(f"raw precision : {precision}")
    print(f"raw recall : {recall}")
    print(f"raw f1-score : {f1}")

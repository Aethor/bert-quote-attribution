import torch
import numpy as np
from copy import deepcopy
from tqdm.auto import tqdm

from datas import Document, Dataset, Quote
from model import (
    SpeakerPredictorModel,
    SpeakerPredictorModelConfig,
    RawSpeakerPredictorModel,
)
from utils import sliding_window
from static import get_tokenizer, get_device


def predict_speakers(
    model: SpeakerPredictorModel, dataset: Dataset, threshold: float = 0.5
) -> Dataset:
    """
    Annotate the input dataset with speaker predictions from the specified raw model.

    :param model: the trained model used for predictions
    :param dataset: the dataset that will be annotated. In particular, the 'predicted_speaker' attribute will be modified.
    :param threshold: prediction threshold
    :return: the modified dataset. This is the same as the input dataset, which is modified by side effect.
    """
    device = get_device()
    model.raw_model.to(device)
    model.raw_model.eval()

    for chapter in tqdm(dataset.chapters):

        for quote in chapter.quotes:

            speakers = chapter.document.characters.keys()
            speaker_scores = {}

            for speaker in speakers:

                if (
                    quote.context_size() < quote.quote_size()
                    or len(quote.closest_speaker_mentions(speaker, 1)) == 0
                ):
                    speaker_scores[speaker] = 0
                    continue

                (
                    quote_context,
                    attention_mask,
                    quote_span_coords,
                    speaker_span_coords,
                    is_speaker,
                ) = quote.prepare(speaker, model.config.speaker_repr_nb)

                with torch.no_grad():
                    outputs, _ = model.raw_model(
                        quote_context.unsqueeze(0).to(device),
                        attention_mask.unsqueeze(0).to(device),
                        quote_span_coords.unsqueeze(0).to(device),
                        speaker_span_coords.unsqueeze(0).to(device),
                    )
                    preds = torch.softmax(outputs, dim=1)
                    speaker_scores[speaker] = preds[0][1].item()

            best_speaker = max(speaker_scores, key=speaker_scores.get)

            if best_speaker and speaker_scores[best_speaker] >= threshold:
                quote.predicted_speaker = best_speaker
            else:
                quote.predicted_speaker = None
            quote.predicted_speaker_scores = speaker_scores

    return dataset


def predict_addressees(dataset: Dataset, use_gold_speaker: bool = False) -> Dataset:
    """
    Annotate the dataset with addressees labels, using a simple heuristic.

    :param dataset: the dataset that will be annotated. In particular, the 'predicted_addressees' attribute will be modified.
    :param use_gold_speaker: if True, use gold speaker datas for prediction
    :return: the modified dataset. This is the same as the input dataset, which is modified by side effect.
    """

    def get_speaker(quote: Quote):
        if quote is None:
            return None
        return quote.speaker if use_gold_speaker else quote.predicted_speaker

    for chapter in dataset.chapters:

        for quotes in sliding_window(chapter.quotes, n=5, padding=2):

            cur_quote = quotes[2]
            if cur_quote is None:
                continue

            cur_quote.predicted_addressees = set(
                [
                    get_speaker(q)
                    for q in quotes
                    if not get_speaker(q) is None
                    and not get_speaker(q) == get_speaker(cur_quote)
                ]
            )

    return dataset

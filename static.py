import torch
from transformers import BertTokenizer
from transformers.tokenization_utils import PreTrainedTokenizer


class Static:
    """
    Class containing all static members
    """

    tokenizer = None
    device = None


def get_tokenizer(bert_tokenizer_type: str = "bert-base-cased") -> PreTrainedTokenizer:
    """
    Get the static bert tokenizer. If it is not initialized, initialize a tokenizer of type bert_tokenizer_type
    
    :param bert_tokenizer_type: bert tokenizer type, as in huggingface transformers
    :return: the initialized static bert tokenizer
    """
    if Static.tokenizer is None:
        Static.tokenizer = BertTokenizer.from_pretrained(bert_tokenizer_type)
    return Static.tokenizer


def get_device(device_type: str = "auto") -> torch.device:
    """
    Get the static torch device. If it is not initialized, initialize a torch device of type device_type
    
    :param device_type: torch device type. Should be one of "auto", "cpu" or "cuda". When "auto", uses "cuda" if possible, otherwise fallback to "cpu".
    :return: the initialized torch device
    """
    if Static.device is None:
        if device_type == "auto":
            Static.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        elif device_type == "cuda":
            Static.device = torch.device("cuda")
        elif device_type == "cpu":
            Static.device = torch.device("cpu")
        else:
            raise Exception(f"Unknown device type : {device_type}")
    return Static.device

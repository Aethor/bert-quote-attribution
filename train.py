import os

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"


import json

import torch
from torch.utils.tensorboard import SummaryWriter
from transformers import AdamW
from tqdm import tqdm

from datas import Document, Dataset
from config import get_args, train_arg_definitions
from predict import predict_speakers
from score import score_speakers
from model import (
    RawSpeakerPredictorModel,
    SpeakerPredictorModelConfig,
    SpeakerPredictorModel,
)
from static import get_tokenizer, get_device


def train_model(
    model: SpeakerPredictorModel,
    train_dataset: Dataset,
    valid_dataset: Dataset,
    optimizer,
    epochs_nb: int,
    batch_size: int,
) -> SpeakerPredictorModel:
    """
    Train the given model
    """

    print("starting training run")

    writer = SummaryWriter()
    device = get_device()

    model.raw_model.to(device)

    loss_fn = torch.nn.CrossEntropyLoss()

    for epoch in range(epochs_nb):

        full_batches_nb = train_dataset.batches_nb(
            model.config.quote_context_size, model.config.speaker_repr_nb, batch_size
        )
        batch_progress = tqdm(
            train_dataset.batches(
                model.config.quote_context_size,
                model.config.speaker_repr_nb,
                batch_size,
            ),
            total=full_batches_nb,
            unit="batch",
        )

        losses = []

        for batch_i, batch in enumerate(batch_progress):
            model.raw_model.train()
            optimizer.zero_grad()

            outputs = model.raw_model(
                batch.quote_context.to(device),
                batch.quote_context_attention_mask.to(device),
                batch.quote_span_coords.to(device),
                batch.speaker_span_coords.to(device),
            )[0]

            loss = loss_fn(outputs, batch.is_speaker.to(device))

            loss.backward()
            optimizer.step()

            writer.add_scalar("loss", loss.item(), epoch * batch_size + batch_i)

            batch_progress.set_description(
                "[e:{}][l:{:0.4f}]".format(epoch + 1, round(loss.item(), 4))
            )

            losses.append(loss.item())

        tqdm.write(
            "[e:{}][mean loss : {:0.4f}]".format(
                epoch + 1, round(sum(losses) / len(losses), 4)
            )
        )

        valid_dataset = predict_speakers(model, valid_dataset, threshold=0)

        precision, recall, f1 = score_speakers(valid_dataset)

        valid_dataset.reset_predictions()

        if not precision is None:
            writer.add_scalar("raw model precision", precision, epoch)
        if not recall is None:
            writer.add_scalar("raw model recall", recall, epoch)
        if not f1 is None:
            writer.add_scalar("raw model f1", f1, epoch)

        tqdm.write(f"raw model validation precision : {precision}")
        tqdm.write(f"raw model validation recall : {recall}")
        tqdm.write(f"raw model validation f1 : {f1}")

    writer.close()

    print("done training raw model !")

    return model


if __name__ == "__main__":

    args = get_args(train_arg_definitions)

    print(f"running with config : {json.dumps(vars(args), indent=4)}")
    # initialize static fields
    get_device(args.device)
    get_tokenizer(args.bert_tokenizer_type)

    train_dataset = Dataset(
        Document.documents_from_descriptions(
            args.training_corpus, args.quote_context_size
        )
    )
    score_dataset = Dataset(
        Document.documents_from_descriptions(
            args.scoring_corpus, args.quote_context_size
        )
    )

    model_config = SpeakerPredictorModelConfig(
        args.quote_context_size,
        args.speaker_repr_nb,
        args.lstm_layers_nb,
        args.lstm_hidden_size,
        args.bert_model_type,
    )
    raw_model = RawSpeakerPredictorModel(model_config)
    model = SpeakerPredictorModel(raw_model, model_config)

    optimizer = AdamW(model.raw_model.parameters(), lr=args.learning_rate)

    model = train_model(
        model, train_dataset, score_dataset, optimizer, args.epochs_nb, args.batch_size
    )

    model.save(args.model_path)

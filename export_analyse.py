import argparse
import os
import sys
import json

from transformers import BertTokenizer
import torch
import nltk
from nltk.sentiment.vader import SentimentIntensityAnalyzer

from datas import Document, Dataset
from predict import predict_speakers, predict_addressees
from config import user_arg_definitions, get_args, ArgDefinition
from model import SpeakerPredictorModel
from static import get_tokenizer, get_device


if __name__ == "__main__":

    nltk.download("vader_lexicon")

    arg_addressees_use_gold_labels = ArgDefinition(
        "-augl",
        "--addressees-use-gold-labels",
        {
            "action": "store_true",
            "help": "wether to use gold speaker labels for addressees prediction",
        },
    )

    args = get_args(user_arg_definitions.union(set([arg_addressees_use_gold_labels])))

    input_file = args.input_files[0]
    if not os.path.isfile(input_file):
        print(f"[error] non-existent input file : {input_file}", file=sys.stderr)
        exit()

    # initializing static fields
    get_tokenizer(args.bert_tokenizer_type)
    get_device(args.device)

    model = SpeakerPredictorModel.load(args.model_path)

    dataset = Dataset([Document(input_file, args.quote_context_size)])

    dataset = predict_speakers(model, dataset, args.threshold)
    dataset = predict_addressees(dataset, args.addressees_use_gold_labels)

    sentiment_analyzer = SentimentIntensityAnalyzer()
    characters = list(dataset.docs[0].characters.keys())
    document = dataset.docs[0]
    quotes = document.quotes

    to_dump = {
        "characters": [
            {
                "name": ch,
                "interactionsCount": len(
                    [
                        q
                        for q in quotes
                        if q.predicted_speaker == ch or ch in q.predicted_addressees
                    ]
                ),
            }
            for ch in characters
        ],
        "interactions": [],
    }

    for quote in dataset.docs[0].quotes:

        if quote.predicted_speaker is None or len(quote.predicted_addressees) == 0:
            continue

        polarity = sentiment_analyzer.polarity_scores(quote.text)["compound"]
        if polarity == 0:
            continue

        to_dump["interactions"].append(
            {
                "activeCharacter": quote.predicted_speaker,
                "targetCharacters": [
                    {"name": addressee, "influence": polarity}
                    for addressee in quote.predicted_addressees
                ],
            }
        )

    print(json.dumps(to_dump, indent=4))

from typing import List, Optional, Set
import argparse
from argparse import Namespace, ArgumentDefaultsHelpFormatter


class Defaults:
    """
    Set of defaults for command line arguments
    """

    training_corpus: List[str] = ["datas/emma.xml"]
    scoring_corpus: List[str] = ["datas/steppe.xml"]
    input_files: List[str] = []
    model_path: str = "models/default"
    quote_context_size: int = 512
    speaker_repr_nb: int = 8
    lstm_layers_nb: int = 1
    lstm_hidden_size: int = 128
    epochs_nb: int = 5
    learning_rate: float = 1e-5
    bert_model_type: str = "SpanBERT/spanbert-base-cased"
    bert_tokenizer_type: str = "bert-base-cased"
    device: str = "auto"
    batch_size: int = 2
    threshold: float = 0.5


class ArgDefinition:
    def __init__(self, short_name: str, long_name: str, parameters: dict):
        self.short_name = short_name
        self.long_name = long_name
        self.parameters = parameters


arg_training_corpus = ArgDefinition(
    "-tc",
    "--training-corpus",
    {
        "nargs": "*",
        "default": Defaults.training_corpus,
        "help": "Document list used when training. each document can be specified with the syntax 'path[chapter slice]' (example : './datas/emma.xml[0:1]')",
    },
)
arg_scoring_corpus = ArgDefinition(
    "-sc",
    "--scoring-corpus",
    {
        "nargs": "*",
        "default": Defaults.scoring_corpus,
        "help": "Document list used when scoring. each document can be specified with the syntax 'path[chapter slice]' (example : './datas/emma.xml[0:1]')",
    },
)
arg_model_path = ArgDefinition(
    "-mp",
    "--model-path",
    {"type": str, "default": Defaults.model_path, "help": "Model path"},
)
arg_epochs_nb = ArgDefinition(
    "-en",
    "--epochs-nb",
    {"type": int, "default": Defaults.epochs_nb, "help": "Number of epochs"},
)
arg_quote_context_size = ArgDefinition(
    "-qcz",
    "--quote-context-size",
    {
        "type": int,
        "default": Defaults.quote_context_size,
        "help": """Quote context size, in tokens. Includes quote and context.
    Must be a multiple of quote_context_segment_size / 2""",
    },
)
arg_speaker_repr_nb = ArgDefinition(
    "-srn",
    "--speaker-repr-nb",
    {
        "type": int,
        "default": Defaults.speaker_repr_nb,
        "help": "Number of speaker representation for each quote",
    },
)
arg_lstm_layers_nb = ArgDefinition(
    "-lln",
    "--lstm-layers-nb",
    {
        "type": int,
        "default": Defaults.lstm_layers_nb,
        "help": "Number of layer for the quote encoder LSTM",
    },
)
arg_lstm_hidden_size = ArgDefinition(
    "-lhz",
    "--lstm-hidden-size",
    {
        "type": int,
        "default": Defaults.lstm_hidden_size,
        "help": "Hidden size for the quote encoder LSTM",
    },
)
arg_learning_rate = ArgDefinition(
    "-lr",
    "--learning-rate",
    {"type": float, "default": Defaults.learning_rate, "help": "Learning rate."},
)
arg_bert_model_type = ArgDefinition(
    "-bmt",
    "--bert-model-type",
    {"type": str, "default": Defaults.bert_model_type, "help": "Bert model type"},
)
arg_bert_tokenizer_type = ArgDefinition(
    "-btt",
    "--bert-tokenizer-type",
    {
        "type": str,
        "default": Defaults.bert_tokenizer_type,
        "help": "Bert tokenizer type",
    },
)
arg_device = ArgDefinition(
    "-d",
    "--device",
    {
        "type": str,
        "default": Defaults.device,
        "help": "Device type (one of 'auto', 'cuda' or 'cpu')",
    },
)
arg_batch_size = ArgDefinition(
    "-bz",
    "--batch-size",
    {"type": int, "default": Defaults.batch_size, "help": "Batch size, in sequences"},
)
arg_threshold = ArgDefinition(
    "-th",
    "--threshold",
    {"type": float, "default": Defaults.threshold, "help": "Prediction threshold"},
)


train_arg_definitions: Set[ArgDefinition] = set(
    [
        arg_training_corpus,
        arg_scoring_corpus,
        arg_model_path,
        arg_epochs_nb,
        arg_quote_context_size,
        arg_speaker_repr_nb,
        arg_lstm_layers_nb,
        arg_lstm_hidden_size,
        arg_learning_rate,
        arg_bert_model_type,
        arg_bert_tokenizer_type,
        arg_device,
        arg_batch_size,
        arg_threshold,
    ]
)

arg_input_files = ArgDefinition(
    "-if",
    "--input-files",
    {
        "nargs": "*",
        "default": Defaults.input_files,
        "help": "Document list used as input. each document can be specified with the syntax 'path[chapter slice]' (example : './datas/emma.xml[0:1]')",
    },
)
user_arg_definitions: Set[ArgDefinition] = set(
    [
        arg_input_files,
        arg_model_path,
        arg_quote_context_size,
        arg_bert_model_type,
        arg_bert_tokenizer_type,
        arg_device,
        arg_threshold,
    ]
)


def get_args(arg_definitions: Optional[Set[ArgDefinition]] = None) -> Namespace:
    """
    get a Namespace object using the specified argument set

    :param arg_definitions: argument set. If None, uses user_arg_definitions for convenience
    :return: an argparse Namespace object
    """
    parser = argparse.ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    if arg_definitions is None:
        arg_definitions = user_arg_definitions
    for arg_definition in arg_definitions:
        parser.add_argument(
            arg_definition.short_name,
            arg_definition.long_name,
            **arg_definition.parameters
        )
    return parser.parse_args()

\documentclass[11pt]{article}
\usepackage{coling2020}
\usepackage{graphicx}
\usepackage{times}
\usepackage{url}
\usepackage{amsmath}
\usepackage{latexsym}
\usepackage{textcomp}


%\setlength\titlebox{5cm}
%\colingfinalcopy % Uncomment this line for the final submission

% You can expand the titlebox if you need extra space
% to show all the authors. Please do not make the titlebox
% smaller than 5cm (the original size); we will check this
% in the camera-ready version and ask you to change it back.

\title{BERT for quote attribution in literary text}

\author{Arthur Amalvy}

\date{}

\begin{document}
\maketitle
\begin{abstract}
  Quote attribution is the task of picking the correct speaker for each quote of a text. In this work, we focus on this task in literary texts, with the aim of supporting character relationships informations extraction systems. We show that a very simple method based on BERT fine-tuning can achieve decent results on this task. This is in contrast with previous work, which used sets of manually crafted rules or machine learning approaches requiring feature engineering.
\end{abstract}

\section{Introduction}
\label{intro}

% The following footnote without marker is needed for the camera-ready
% version of the paper.
% Comment out the instructions (first text) and uncomment the 8 lines
% under "final paper" for your variant of English.
\blfootnote{
    % for review submission
    \hspace{-0.65cm}  % space normally used by the marker
    Place licence statement here for the camera-ready version.
    % % final paper: en-uk version 
    % \hspace{-0.65cm}  % space normally used by the marker
    % This work is licensed under a Creative Commons 
    % Attribution 4.0 International Licence.
    % Licence details:
    % \url{http://creativecommons.org/licenses/by/4.0/}.
    % 
    % % final paper: en-us version 
    % \hspace{-0.65cm}  % space normally used by the marker
    % This work is licensed under a Creative Commons 
    % Attribution 4.0 International License.
    % License details:
    % \url{http://creativecommons.org/licenses/by/4.0/}.
}

Dialogues play a central role in literary texts, especially in regards to characters and their relationships. Constructing automating tools allowing to study those relationships would therefore benefit from automatic informations extraction in dialogues.  However, implementing systems to automatically extract those informations is not an easy task due to the number of subtasks involved. One of the most important of these subtasks is quote attribution : Given a set of quotes, the task is to predict a speaker for each of them.
Prior work on quote attribution often focus on classical machine learning approaches and/or hand-crafted rules. While recent systems report high accuracy \cite{muzny:2017} \cite{he:2013}, their manually-crafted nature have several drawbacks :
\begin{itemize}
\item Enhancing those systems implies crafting more rules or discovering more features, both of which may be difficult problems
\item The same set of rules or features may not be optimal when used in the context of different literary styles, let alone different types of text
\end{itemize}
Meanwhile, research in Natural Language Processing has recently shifted towards the use of transformer based models, language understanding and transfer learning. Models such as BERT \cite{devlin:2019} have been shown to improve the state of the art of a wide variety of NLP tasks. Therefore, to alleviate previously described problems as well as pave the way for future research, we design an approach based on BERT fine-tuning to perform quote attribution.

\section{Related Work}

\newcite{glass:2007} obtain high accuracy on literary texts using a three-phase hand-crafted rule system with no training. However, their system only works when a speech verb is explicitely associated with a quote, and therefore limited.

\newcite{elson:2010} use a machine learning approach to perform quote attribution, but construct their feature vectors using unrealistic gold standard features. Instead of linking a quote to its speaker directly, they link it to a mention that refers to the speaker.

\newcite{okeefe:2012} also consider quote attribution at the mention level, but use a sequence labelling approach. The huge difference between their results on newswire (92.4 accuracy) and literary texts (53.3 accuracy) suggests that quote attribution systems should be adapted to different domains in order to be successful.

\newcite{he:2013} model quote attribution at the speaker level, and use a supervised machine learning approach. By using SVM-rank\cite{joachims:2006} to rank speaker candidates and proposing new features, they report an accuracy as high as 82.5\% on \emph{Pride and Prejudice} from Jane Austen. However, as noted in \newcite{muzny:2017}, their scores are based on an easier subset of quotes.

Lastly, \newcite{muzny:2017} design a two-stage system : Their first system links a quote with its mention, while their second one links this mention to a speaker. Both systems consist of a set of sieves, the ordering of which can be modified to benefit either precision or recall. Their work is now included in the Stanford CoreNLP framework. They also released the QuoteLi3 dataset, comprised of three novels annotated with speakers and mentions labels. However, their concept of quote mention lacks a precise definition\footnote{Annotators were instructed to link a quote with 'a mention that is the most obvious indiciation of that quote speaker'}. If we consider the following example :

\vspace{10px}

\texttt{\textbf{He} was tired, and his thoughts were racing.}

\texttt{- \emph{"This cannot go on."}}

\texttt{But \textbf{he} had to go on.}

\vspace{10px}

It is unclear wether the first or the second \emph{he} should be considered as the quote mention. This renders some choices of mentions somewhat arbitrary, when a system might be able to make use of both mentions for greater performance. As a result, although this work makes use of the QuoteLi3 dataset, it concentrates solely on quote attribution at the speaker level and entirely ignore mention annotations. 

\section{Model}

For each quote $q_t$ of a given text, the goal of the quote attribution task is to choose a speaker $p_{i}$ in the set of all possible speakers $P$. As noted by \newcite{he:2013}, a challenge of quote attribution lies in the fact that this set of possible speakers $P$ is unique to each text. To perform the task of quote attribution in a general setting, our model takes as input a quote representation $f(q_t)$ and a candidate speaker representation $g(q_t,p_i)$, and outputs a score $s_{ti}$ between 0 and 1. By computing this score for each speaker $p_i$, we can simply predict the speaker of the quote $q_t$ to be $max_i(s_{ti})$. To be able to adjust precision and recall, we set an adjustable threshold : If the best score $max_i(s_{ti})$ is below that threshold, we do not predict any speaker.

We define the context $c_t$ of a quote to be a list of tokens $o_1...o_n$, where the quote itself is composed of tokens $o_k...o_l$. We set the numbers of left tokens $k$ to be equal to the numbers of right tokens $n - l$, while we consider $n$ as an hyperparameter. To generate quote and candidate speaker representations, we start by encoding the whole context of the quote using BERT, generating $e_1...e_n$. Then, the quote representation $f(q_t)$ is defined as :

\begin{gather}
b_t = [e_k;e_l] \\
z_t = \frac{1}{l-k} \sum_{i=k}^l{BILSTM(e_k...e_l)_i} \\
f(q_t) = [b_t;z_t]
\end{gather}

where :
\begin{itemize}
\item $;$ represents the concatenation operation
\item $b_t$ is the concatenation of the first and last tokens encoding of the quote
\item $z_t$ is the mean of tokens encoding processed by a bidirectional LSTM
\end{itemize}

To generate a speaker representation, we consider that each speaker possess a list of aliases $A_i$ by which they are referred troughout the text. Although the extraction of those aliases is an interesting problem on its own, it is outside of the scope of this work. For a speaker $p_i$, we generate its representation by using the set $Y$ of the $m$ encoded spans exactly matching an alias of $A_i$ closest to the quote in its encoded context (treating $m$ as an hyperparameter). The speaker representation is then simply :

\begin{equation}
g(q_t,p_i) = [y_1;...;y_m]
\end{equation}

The score of a candidate speaker $p_i$ for a quote $q_t$ is then :

\begin{equation}
s_{ti} = softmax(FFN([f(q_t);g(q_t,p_i)]))_2
\end{equation}

where $FFN$ denotes a feed-forward neural network of output size 2.


\section{Experiments}

To train and evaluate our model, we use the QuoteLi3 dataset from \newcite{muzny:2017}. It is comprised of 3 annotated novels :

\begin{itemize}
\item \emph{Emma}, from Jane Austen ;
\item \emph{Pride and Prejudice}, also from Jane Austen ;
\item \emph{Steppe}, from Anton Chekhov.
\end{itemize}

We implement our model using the pytorch framework \cite{paszke:2019} and the transformers library \cite{wolf:2019}. We use the AdamW optimizer \cite{loshchilov:2017}, a learning rate of $5e-6$ in all layers, and we set our bidirectionnal LSTM from equation 2 to have 3 layers. We finetune our model during 7 epochs. For each experiment, we train our model on two novels, and score it on the third.

\section{Results}

\begin{table}
  \centering
  \begin{tabular}{ c c | c | c | c }
    model                  & novel                 & precision & recall & F1-score \\
    \hline
    \hline
    \emph{ours}            & Pride and Prejudice   &  65.46    &  51.62 & 57.72    \\
    \emph{ours}            & Emma                  &  62.76    &  44.07 & 52.52    \\
    \emph{ours}            & The Steppe            &  81.79    &  45.26 & 58.27    \\
    \hline
    \emph{ours (SpanBERT)} & Pride and Prejudice   &  83.97    &  51.87 & 64.13    \\
    \emph{ours (SpanBERT)} & Emma                  &  68.11    &  48.65 & 56.76    \\
    \emph{ours (SpanBERT)} & The Steppe            &  85.28    &  61.75 & 71.63    \\
    \hline
    \emph{ours (+heuristic)}            & Pride and Prejudice   & 57.97     & 57.97  & 57.97   \\
    \emph{ours (+heuristic)}            & Emma                  & 53.74     & 51.35  & 52.52   \\
    \emph{ours (+heuristic)}            & The Steppe            & 55.95     & 55.04  & 55.98   \\
    \hline                                                             
    \emph{ours (SpanBERT)(+heuristic)}  & Pride and Prejudice   & 64.20     & 62.86  & 63.52   \\
    \emph{ours (SpanBERT)(+heuristic)}  & Emma                  & 57.34     & 55.26  & 56.28   \\
    \emph{ours (SpanBERT)(+heuristic)}  & The Steppe            & 68.14     & 67.45  & 67.79   \\
  \end{tabular}
  \caption{Quote attribution performance on the QuoteLi3 dataset}
  \label{table:scores}
\end{table}

The results of our experiments can be seen in Table \ref{table:scores}, for a threshold of $0.5$. Given the recent success of SpanBERT in coreference resolution \cite{joshi:2019:coref} \cite{joshi:2019:spanbert}, which requires to generate mention representations, we also report results using this model in place of BERT as \emph{ours (SpanBERT)}. 

Although our model aims for simplicity by not using hand-crafted features, we attempt to enhance recall with a simple approach : when our system cannot identify a speaker for a given quote $q_t$ (all speaker scores being below threshold), we assign the speaker of this quote to be the speaker predicted for the quote $q_{t-2}$ if possible. This stems from the intuition that conversations in novels tend to follow a pattern where two characters take turn talking to each other\footnote{This pattern is sometimes called the speaker alternation pattern}. When this pattern is applied, hints giving away speakers identities are often found in the first and second quote of a conversation. We label this variation of our system as \emph{+heuristic}. 

Unfortunately, it is difficult to compare our results with previous work. \newcite{muzny:2017} notes that the work of \newcite{he:2013} uses an easier subset of quotes for evaluation, and that \newcite{elson:2010} uses gold data at test time. Meanwhile, the work of \newcite{muzny:2017} itself does not provide the speaker attribution score directly, instead reporting the score of both stages of their two-stage approach.

Our SpanBERT version performs better everywhere, which tends to show that span representation is indeed an important subtask of our system.

Interestingly, the threshold of our model can be set to benefit either precision (high threshold) or recall (low threshold). Figure \ref{fig:precision_vs_recall} shows a precision versus recall curve for \emph{Emma}.

\begin{figure}
  \includegraphics[width=0.75\linewidth]{precision_vs_recall.png}
  \caption{Precision VS Recall curve for Emma. The red curve stands for our basic system, while the blue curve stands for our (+heuristic) system.}
  \label{fig:precision_vs_recall}
\end{figure}

\section{Further Analysis}

In order to better understand our model, we build an attention visualisation tool. We qualitatively compare attention between a raw SpanBERT model and our best fine-tuned model. We make the following observations :

\begin{itemize}
\item Some attentions heads of the fine-tuned model seems to be performing coreference resolution, which may allow the model to better identify a speaker
\item In general, the fine-tuned model pays more attention to quote delimiters and speech verbs
\end{itemize}

\section{Conclusion}

In this work, we showed that a system based on finetuning BERT can achieve decent results in the task of quote attribution. In contrast to previous work, we do not use hand-crafted rules or features, making our system easy to understand and implement.
Although quote attribution is an important task to be able to extract information from dialogues in literary texts, it is far from the only one. Quote adressees attribution is a problem linked to quote attribution \cite{ek:2018}, sentiment analysis might be interesting to define relationships, while automatic extraction of character lists and aliases \cite{vala:2015} is also needed to fully automate this work.


\newpage
\nocite{*}
\bibliographystyle{coling}
\bibliography{biblio}

\end{document}

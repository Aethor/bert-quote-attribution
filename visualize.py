from typing import Optional, List, Union
from enum import Enum

import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Pango

from transformers import BertTokenizer
import torch

from config import get_args, user_arg_definitions
from datas import Document, Chapter, Quote
from model import SpeakerPredictorModel
from static import get_tokenizer, get_device


def color_from_weight(weight: float, center: str = "red") -> str:
    """
    Generate a color from an attention weight

    :param weight: float between 0 and 1
    :param center: one of "red", "blue" or "green"
    :return: corresponding color as an hex string
    """
    if center == "red":
        rgb = (255, int(200 - weight * 200), int(125 - weight * 125))
    elif center == "blue":
        rgb = (int(125 - weight * 125), int(200 - weight * 200), 255)
    elif center == "green":
        rgb = (int(125 - weight * 125), 255, int(200 - weight * 200))
    else:
        raise Exception
    return "#%02x%02x%02x" % rgb


class Visualizer:
    """
    Visualizer class
    """
    def __init__(self, cli_args):
        """
        :param cli_args: command line arguments, generated using :func:`get_args`
        """
        self.builder = Gtk.Builder.new_from_file("visualizer.glade")
        self.main_window = self.builder.get_object("main_window")
        self.main_window.connect("destroy", Gtk.main_quit)

        self.quote_context_size = cli_args.quote_context_size

        print("loading model...")
        self.model = SpeakerPredictorModel.load(cli_args.model_path)

        self.cur_doc: Optional[Document] = None
        self.cur_chapter: Optional[Chapter] = None
        self.cur_chapter_i: Optional[int] = None
        self.cur_quote: Optional[Quote] = None
        self.cur_quote_i: Optional[int] = None

        self.cur_context_attentions: Optional[torch.Tensor] = None
        self.context_attentions_heads_nb = (
            self.model.raw_model.quote_encoder.bert_encoder.heads_nb
        )
        self.cur_context_attentions_head: Union[int, str] = 0
        self.cur_highlighted_token: int = 0

        self.next_quote_button = self.builder.get_object("next_quote_button")
        self.next_quote_button.connect("clicked", self.next_quote)

        self.prev_quote_button = self.builder.get_object("prev_quote_button")
        self.prev_quote_button.connect("clicked", self.prev_quote)

        self.next_chapter_button = self.builder.get_object("next_chapter_button")
        self.next_chapter_button.connect("clicked", self.next_chapter)

        self.prev_chapter_button = self.builder.get_object("prev_chapter_button")
        self.prev_chapter_button.connect("clicked", self.prev_chapter)

        self.predict_button = self.builder.get_object("predict_button")
        self.predict_button.connect("clicked", self.predict_cur_quote)

        self.context_attentions_head_cbox = self.builder.get_object(
            "context_attentions_head_cbox"
        )
        for i in range(self.context_attentions_heads_nb):
            self.context_attentions_head_cbox.append_text(str(i))
        self.context_attentions_head_cbox.append_text("mean")
        self.context_attentions_head_cbox.connect("changed", self.attention_head_change)

        self.quote_text_label = self.builder.get_object("quote_text_label")
        self.quote_text_label_event_box = self.builder.get_object(
            "quote_text_label_event_box"
        )
        self.quote_text_label_event_box.connect(
            "motion-notify-event", self.highlight_token
        )

        self.result_label = self.builder.get_object("result_label")

        self.chapter_i_label = self.builder.get_object("chapter_i_label")
        self.quote_i_label = self.builder.get_object("quote_i_label")

        self.open_menu_item = self.builder.get_object("open_menu_item")
        self.open_menu_item.connect("activate", self.open_file)

        self.quit_menu_item = self.builder.get_object("quit_menu_item")
        self.quit_menu_item.connect("activate", Gtk.main_quit)

        self.main_window.show_all()
        Gtk.main()

    def next_quote(self, *args, **kwargs):
        """
        Set the :code:`self.cur_quote` attribute to the next chapter quote if it exists
        """
        if self.cur_chapter is None:
            return
        if len(self.cur_chapter.quotes) - 1 == self.cur_quote_i:
            return
        self.set_cur_quote(self.cur_quote_i + 1)

    def prev_quote(self, *args, **kwargs):
        """
        Set the :code:`self.cur_quote` attribute to the previous chapter quote if it exists
        """
        if self.cur_chapter is None:
            return
        if self.cur_quote_i == 0:
            return
        self.set_cur_quote(self.cur_quote_i - 1)

    def set_cur_quote(self, index: Optional[int]):
        """
        Set the current quote
        """
        if self.cur_chapter is None:
            return
        if index < 0:
            return
        if index >= len(self.cur_chapter.quotes):
            return
        self.cur_quote_i = index
        self.quote_i_label.set_text(
            "quote {}/{}".format(index + 1, len(self.cur_chapter.quotes))
        )
        self.cur_quote = self.cur_chapter.quotes[self.cur_quote_i]
        self.cur_context_attentions = None
        self.cur_highlighted_token = 0
        self.cur_context_attentions_head = 0
        self.display_cur_quote()

    def set_cur_chapter(self, index: int):
        """
        Set the curent chapter
        """
        if self.cur_doc is None:
            return
        if index >= len(self.cur_doc.chapters):
            return
        if index < 0:
            return
        self.cur_chapter_i = index
        self.chapter_i_label.set_text(
            "chapter {}/{}".format(index + 1, len(self.cur_doc.chapters))
        )
        self.cur_chapter = self.cur_doc.chapters[self.cur_chapter_i]
        self.set_cur_quote(0)

    def next_chapter(self, *args, **kwargs):
        """
        Set the :code:`self.cur_chapter` attribute to the next chapter if it exists
        """
        if self.cur_chapter_i is None:
            return
        self.set_cur_chapter(self.cur_chapter_i + 1)

    def prev_chapter(self, *args, **kwargs):
        """
        Set the :code:`self.cur_chapter` attribute to the previous chapter if it exists
        """
        if self.cur_chapter_i is None:
            return
        self.set_cur_chapter(self.cur_chapter_i - 1)

    def highlight_token(self, widget, event, *args, **kwargs):
        """
        Highlight the token under mouse and other tokens with their respective attention
        """
        if self.cur_quote is None:
            return

        pango_layout = self.quote_text_label.get_layout()
        pango_layout_offset = self.quote_text_label.get_layout_offsets()
        mouse_position = event

        layout_position = (
            mouse_position.x - pango_layout_offset.x,
            mouse_position.y - pango_layout_offset.y,
        )
        highlighted_char_idx = pango_layout.xy_to_index(
            layout_position[0] * Pango.SCALE, layout_position[1] * Pango.SCALE
        ).index_

        cur_char_idx = 0

        for token_i, token in enumerate(
            ["[CLS]"] + self.cur_quote.context_tokens + ["[SEP]"]
        ):

            cur_char_idx += len(token) + 1

            if cur_char_idx >= highlighted_char_idx:
                self.cur_highlighted_token = token_i

                self.display_cur_context_attentions()

                return

    def attention_head_change(self, combo):
        """
        Refresh display after an UI change of attention head
        """
        tree_iter = combo.get_active_iter()
        if tree_iter is None:
            return

        model = combo.get_model()
        cur_context_attentions_head = model[tree_iter][0]

        if cur_context_attentions_head != "mean":
            self.cur_context_attentions_head = int(cur_context_attentions_head)
        else:
            self.cur_context_attentions_head = "mean"

        self.display_cur_context_attentions()

    def display_cur_context_attentions(self):
        """
        Call :func:`self.display_cur_quote` with tag to display attention
        """
        if self.cur_quote is None or self.cur_context_attentions is None:
            return

        token_attention_tags = []

        for token_i, token in enumerate(
            ["[CLS]"] + self.cur_quote.context_tokens + ["[SEP]"]
        ):

            if self.cur_context_attentions_head == "mean":
                token_weight = torch.mean(
                    self.cur_context_attentions[:, token_i, self.cur_highlighted_token]
                ).item()
            else:
                token_weight = self.cur_context_attentions[
                    self.cur_context_attentions_head
                ][token_i][self.cur_highlighted_token].item()

            if token_i == self.cur_highlighted_token:
                color_center = "green"
            elif (
                token_i >= self.cur_quote.local_quote_coords()[0]
                and token_i < self.cur_quote.local_quote_coords()[1]
            ):
                color_center = "blue"
            else:
                color_center = "red"

            color_str = color_from_weight(token_weight, center=color_center)

            token_attention_tags.append(
                {"index": token_i, "text": f'<span background="{color_str}">'}
            )
            token_attention_tags.append({"index": token_i + 1, "text": f"</span>"})

        self.display_cur_quote(token_attention_tags)

    def display_cur_quote(self, tags: Optional[List[dict]] = None):
        """
        Display the current quote 

        :param tags: A list of dict of form :code:`{"index": int, "text": str}` to be applied as text special tags
        """
        if self.cur_quote is None:
            return

        quote_span_coords = self.cur_quote.local_quote_coords(
            self.cur_quote.context_chapter_coords[0]
        )
        if tags is None:
            tags = [
                {"index": quote_span_coords[0], "text": "<b>"},
                {"index": quote_span_coords[1], "text": "</b>"},
            ]

        tags.sort(key=lambda e: e["index"])
        markup_list = []
        for token_i, token in enumerate(
            ["[CLS]"] + self.cur_quote.context_tokens + ["[SEP]"]
        ):
            while len(tags) > 0 and token_i == tags[0]["index"]:
                markup_list.append(tags[0]["text"])
                tags.pop(0)
            markup_list.append(token + " ")
        # remaining tags
        for tag in tags:
            markup_list.append(tag["text"])

        self.quote_text_label.set_markup("".join(markup_list))

    def predict_cur_quote(self, *args, **kwargs):
        """
        Predict the speaker of the current quote.
        Also sets attention attribute :code:`cur_context_attentions`.
        Call :func:`display_cur_context_attention` to display the prediction
        """
        if self.cur_quote is None or self.cur_chapter is None:
            return

        device = get_device()
        self.model.raw_model.to(device)
        self.model.raw_model.eval()

        speakers = self.cur_chapter.document.characters.keys()
        speaker_datas = {speaker: {} for speaker in speakers}
        for speaker in self.cur_chapter.document.characters.keys():
            (
                quote_context,
                attention_mask,
                quote_span_coords,
                speaker_span_coords,
                is_speaker,
            ) = self.cur_quote.prepare(speaker, self.model.config.speaker_repr_nb)
            with torch.no_grad():
                outputs, context_attentions = self.model.raw_model(
                    quote_context.unsqueeze(0).to(device),
                    attention_mask.unsqueeze(0).to(device),
                    quote_span_coords.unsqueeze(0).to(device),
                    speaker_span_coords.unsqueeze(0).to(device),
                )
                preds = torch.softmax(outputs, dim=1)
                speaker_datas[speaker]["score"] = preds[0][1].item()
                speaker_datas[speaker]["context_attentions"] = context_attentions[0][0]

        best_speaker = max(
            [(speaker, datas["score"]) for speaker, datas in speaker_datas.items()],
            key=lambda e: e[1],
        )[0]
        best_score = speaker_datas[best_speaker]["score"]

        self.cur_context_attentions = speaker_datas[best_speaker]["context_attentions"]

        self.display_cur_context_attentions()

        self.result_label.set_text(
            f"true speaker : {self.cur_quote.speaker}\n"
            + f"Predicted speaker : {best_speaker} (score : {best_score})"
        )

    def open_file(self, *args, **kwargs):
        """
        Open a file using a file chooser dialog, by setting the :code:`self.cur_doc` attribute
        """
        file_chooser = Gtk.FileChooserDialog(
            title="Open file...",
            parent=self.main_window,
            action=Gtk.FileChooserAction.OPEN,
        )
        file_chooser.add_buttons(
            Gtk.STOCK_CANCEL,
            Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN,
            Gtk.ResponseType.OK,
        )

        answer = file_chooser.run()
        if answer == Gtk.ResponseType.OK:
            self.cur_doc = Document(
                file_chooser.get_filename(), self.quote_context_size
            )
            if len(self.cur_doc.chapters) != 0:
                self.set_cur_chapter(0)
                if len(self.cur_chapter.quotes) != 0:
                    self.cur_quote_i = 0
                    self.cur_quote = self.cur_chapter.quotes[0]
                    self.display_cur_quote()

        file_chooser.destroy()


if __name__ == "__main__":

    cli_args = get_args(user_arg_definitions)

    # initialize static fields
    get_tokenizer(cli_args.bert_tokenizer_type)
    get_device(cli_args.device)

    visualizer = Visualizer(cli_args)

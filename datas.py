from __future__ import annotations
from typing import Generator, List, Tuple, Optional, Mapping, Union, Set
import os
import re
import copy
from xml.etree import ElementTree as ET

import torch
from tqdm.auto import tqdm

from utils import shuffle_, pad_tensor_1d, pad_list
from static import get_tokenizer


class Mention:
    """
    A speaker mention
    """

    def __init__(self, tokens: List[str], text: str, chapter_coords: Tuple[int, int]):
        self.tokens = tokens
        self.text = text
        self.chapter_coords = chapter_coords

    def local_coords(self, origin: int) -> Tuple[int, int]:
        """
        Coordinates, in tokens, in the current chapter
        """
        return (self.chapter_coords[0] - origin, self.chapter_coords[1] - origin)


class Quote:
    """
    A quote
    """

    def __init__(
        self,
        quote_tokens: List[str],
        text: str,
        chapter_coords: Tuple[int, int],
        speaker: str,
        addressees: Set[str],
        quote_context_size: int,
        chapter: Chapter,
        document: Document,
    ):
        self.quote_tokens = quote_tokens
        self.quote_context_size = quote_context_size
        self.text = text
        self.chapter_coords = chapter_coords

        self.speaker = speaker
        self.addressees = addressees
        self.reset_prediction()

        self.chapter = chapter
        side_context_size = (quote_context_size - len(self.quote_tokens)) // 2 - 2
        self.context_chapter_coords = (
            max(chapter_coords[0] - side_context_size, 0),
            min(chapter_coords[1] + side_context_size, len(chapter.tokens)),
        )
        self.context_tokens = chapter.tokens[
            self.context_chapter_coords[0] : self.context_chapter_coords[1]
        ]

        self.document = document
        self.mentions: List[Mention] = []

    def reset_speaker_prediction(self):
        """
        | Resets speaker predictions by resetting the following attributes to :code:`None` :
        | - :code:`self.predicted_speaker`
        | - :code:`self.predicted_speaker_scores`
        """
        self.predicted_speaker: Optional[str] = None
        self.predicted_speaker_scores: Optional[Mapping[str, float]] = None

    def reset_addressees_prediction(self):
        """
        Resets addressees prediction by setting the :code:`self.predicted_addressees` attributes to the empty set
        """
        self.predicted_addressees: Set[str] = set()

    def reset_prediction(self):
        """
        Reset speaker and addressees predictions using :func:`reset_speaker_predictions` and :func:`reset_addressees_prediction`
        """
        self.reset_speaker_prediction()
        self.reset_addressees_prediction()

    def context_size(self) -> int:
        """
        :return: the size of the quote context, in tokens
        """
        return len(self.context_tokens)

    def quote_size(self) -> int:
        """
        :return: the size of the quote, in tokens
        """
        return len(self.quote_tokens)

    def closest_mentions(self, k: int) -> List[Mention]:
        """
        :return: the k closest mentions
        """
        return sorted(self.mentions, key=lambda m: self.dist(m))[:k]

    def closest_speaker_mentions(self, name: str, k: int) -> List[Mention]:
        """
        :param name: speaker name
        :return: the k closest mentions for the speaker with the specified name. Speaker aliases are used.
        """
        aliases = self.document.characters[name]["aliases"]
        speaker_mentions: List[Mention] = []
        for mention in self.mentions:
            if mention.text == name or mention.text in aliases:
                speaker_mentions.append(mention)
        return sorted(speaker_mentions, key=lambda m: self.dist(m))[:k]

    def dist(self, text_object: Union[Mention, Quote]) -> float:
        """
        :return: the distance, in tokens, between the current quote and the specified mention or quote
        """
        return abs(
            (self.chapter_coords[0] + self.chapter_coords[1]) / 2
            - (text_object.chapter_coords[0] + text_object.chapter_coords[1]) / 2
        )

    def local_quote_coords(self, origin: Optional[int] = None) -> Tuple[int, int]:
        """
        :return: coordinates, in tokens, of the quote in the current chapter
        """
        if origin is None:
            origin = self.context_chapter_coords[0]
        if self.chapter_coords[0] - origin < 0:
            raise Exception
        return (self.chapter_coords[0] - origin, self.chapter_coords[1] - origin)

    def prepare(self, speaker_name: str, speaker_repr_nb: int) -> Tuple[torch.Tensor]:
        """
        Create all tensors needed for the model from the current quote

        :return: | a tuple of tensors :
            | **quote_context**       : (quote_context_size)
            | **attention_mask**      : (quote_context_size)
            | **quote_span_coords**   : (2)
            | **speaker_span_coords** : (speaker_repr_nb, 2)
            | **is_speaker**          : (1)
        """
        tokenizer = get_tokenizer()
        quote_context = torch.tensor(tokenizer.encode(self.context_tokens))
        quote_context, attention_mask = pad_tensor_1d(
            quote_context,
            self.quote_context_size,
            generate_attention_mask=True,
            location="right",
        )
        quote_span_coords = torch.tensor(
            self.local_quote_coords(self.context_chapter_coords[0])
        )
        speaker_span_coords = torch.tensor(
            pad_list(
                [
                    m.local_coords(self.context_chapter_coords[0])
                    for m in self.closest_speaker_mentions(
                        speaker_name, speaker_repr_nb
                    )
                ],
                speaker_repr_nb,
                (-1, -1),
            )
        )
        is_speaker = torch.tensor([1 if self.speaker == speaker_name else 0])
        return (
            quote_context,
            attention_mask,
            quote_span_coords,
            speaker_span_coords,
            is_speaker,
        )


class Chapter:
    """
    A book chapter
    """

    def __init__(self, tokens: List[str], document: Document):
        self.quotes: List[Quote] = list()
        self.mentions: List[Mention] = list()
        self.tokens = tokens
        self.document = document

    def mentions_in_range(self, start: int, end: int) -> List[Mention]:
        """
        :return: all mentions between the given coordinates
        """
        return [
            m
            for m in self.mentions
            if m.chapter_coords[0] >= start and m.chapter_coords[1] < end
        ]


class Document:
    """
    A document represent an entire book
    """

    def __init__(
        self,
        filename: str,
        quote_context_size: int,
        chapter_slice: Optional[Tuple[int, int]] = None,
    ):
        tokenizer = get_tokenizer()
        self.filename = os.path.basename(filename)

        root = ET.parse(filename)

        self.characters = {
            c.attrib["name"]: {
                "aliases": c.attrib["aliases"].split(";"),
                "gender": c.attrib.get("gender", None),
            }
            for c in root.iter("character")
        }

        if chapter_slice is None:
            chapter_nodes = [c for c in root.iter("chapter")]
        else:
            chapter_nodes = [c for c in root.iter("chapter")][
                chapter_slice[0] : chapter_slice[1]
            ]

        self.chapters: List[Chapter] = [
            Chapter(tokenizer.tokenize("".join(c.itertext())), self)
            for c in tqdm(chapter_nodes, desc=f"tokenizing {self.filename} chapters")
        ]
        for i, chapter in tqdm(
            enumerate(chapter_nodes),
            total=len(chapter_nodes),
            desc=f"parsing {self.filename} quotes and mentions",
        ):
            self._parse_xml_(chapter, 0, i, quote_context_size)
        for chapter in self.chapters:
            for quote in chapter.quotes:
                quote.mentions = chapter.mentions_in_range(
                    *quote.context_chapter_coords
                )

        self.quotes: List[Quote] = []
        for chapter in self.chapters:
            for quote in chapter.quotes:
                self.quotes.append(quote)

    def _parse_xml_(
        self, node, current_idx: int, chapter_i: int, quote_context_size: int
    ) -> List[str]:
        tokenizer = get_tokenizer()
        node.attrib["tokens"] = tokenizer.tokenize(
            node.text if not node.text is None else ""
        )

        child_idx = current_idx + len(
            tokenizer.tokenize(node.text if not node.text is None else "")
        )
        for child in [c for c in node.findall("./*") if not c is node]:
            node.attrib["tokens"] += self._parse_xml_(
                child, child_idx, chapter_i, quote_context_size
            )
            tail_tokens = tokenizer.tokenize(
                child.tail.strip() if not child.tail is None else ""
            )
            node.attrib["tokens"] += tail_tokens
            child_idx += len(child.attrib["tokens"]) + len(tail_tokens)

        if node.tag == "quote":
            self.chapters[chapter_i].quotes.append(
                Quote(
                    node.attrib["tokens"],
                    "".join(node.itertext()).strip(),
                    (current_idx, current_idx + len(node.attrib["tokens"])),
                    node.attrib["speaker"],
                    set(node.attrib.get("addressees", "").split(";")),
                    quote_context_size,
                    self.chapters[chapter_i],
                    self,
                )
            )
        elif node.tag == "mention":
            self.chapters[chapter_i].mentions.append(
                Mention(
                    node.attrib["tokens"],
                    "".join(node.itertext()).strip(),
                    (current_idx, current_idx + len(node.attrib["tokens"])),
                )
            )

        return node.attrib["tokens"]

    @staticmethod
    def documents_from_descriptions(
        descriptions: List[str], quote_context_size: int
    ) -> List[Document]:
        """
        Load documents using a description mini-language
        
        The description language for loading document looks like this : 'path1[chapter slice 1] path2[chapter slice 2] ...'
        
        chapter slices have the same syntax as standard python slices.
        Here are some examples :

        * loading all chapters from emma.xml : './datas/emma.xml'
        * loading chapters from 0 included to 3 excluded : './datas/emma.xml[0:3]'
        * loading chapter 1 from emma and chapter 1 from steppe : './datas/emma.xml[0:1] ./datas/steppe.xml[0:1]'
        """
        documents = []
        for description in descriptions:
            path = re.findall(r"^([^\[]*).*$", description)[0]
            chapter_slice_match = re.findall(
                r"^[^\[]*(\[[0-9]+:[0-9]+\])$", description
            )
            if len(chapter_slice_match) == 0:
                documents.append(Document(path, quote_context_size))
            else:
                chapter_slice = (
                    int(chapter_slice_match[0][1:-1].split(":")[0]),
                    int(chapter_slice_match[0][1:-1].split(":")[-1]),
                )
                documents.append(Document(path, quote_context_size, chapter_slice))

        return documents


class Batch:
    """
    A Batch of quotes

    :quote_context:                (batch_size, quote_context_size)
    :quote_context_attention_mask: (batch_size, quote_context_size)
    :quote_span_coords:            (batch_size, 2)
    :speaker_span_coords:          (batch_size, speaker_repr_nb, 2)
    :is_speaker:                   (batch_size)
    """

    def __init__(self, quote_context_size: int, speaker_repr_nb: int, batch_size: int):
        self.quote_context = torch.zeros(
            batch_size, quote_context_size, dtype=torch.long
        )
        self.quote_context_attention_mask = torch.zeros(
            batch_size, quote_context_size, dtype=torch.long
        )
        self.quote_span_coords = torch.zeros(batch_size, 2, dtype=torch.long)
        self.speaker_span_coords = torch.zeros(
            batch_size, speaker_repr_nb, 2, dtype=torch.long
        )
        self.is_speaker = torch.zeros(batch_size, dtype=torch.long)
        self.batch_contents = list(vars(self).values())

        self.batch_i = 0

        if not self.validate_len():
            raise Exception

    def validate_len(self) -> bool:
        return all(
            [v.shape[0] == self.quote_context.shape[0] for v in self.batch_contents]
        )

    def append_(self, *args):
        if not len(args) == len(self.batch_contents):
            raise Exception

        for batch_attr, quote_tens in zip(self.batch_contents, args):
            batch_attr[self.batch_i] = quote_tens
        self.batch_i += 1

        if not self.validate_len():
            raise Exception

    def __len__(self):
        return self.batch_i


class Dataset:
    """
    A collection of Documents
    """

    def __init__(self, docs: List[Document]):
        self.docs = docs
        self.chapters: List[Chapter] = list()
        for doc in self.docs:
            for chapter in doc.chapters:
                self.chapters.append(chapter)

    def reset_predictions(self):
        """
        Reset predictions for all quotes of the Dataset
        """
        for doc in self.docs:
            for quote in doc.quotes:
                quote.reset_prediction()

    def reset_speaker_predictions(self):
        """
        Reset speaker predictions for all quotes of the Dataset
        """
        for doc in self.docs:
            for quote in doc.quotes:
                quote.reset_speaker_prediction()

    def reset_addressees_predictions(self):
        """
        Reset predictions for all quotes of the Dataset
        """
        for doc in self.docs:
            for quote in doc.quotes:
                quote.reset_addressees_prediction()

    def batches_nb(self, *args) -> int:
        """ 
        Temporary batch counter
        
        :note: FIXME
        """
        batch_counter = 0
        for _ in self.batches(*args):
            batch_counter += 1
        return batch_counter

    def batches(
        self, quote_context_size: int, speaker_repr_nb: int, batch_size: int
    ) -> Generator[Batch, None, None]:
        """
        Generator over batches for a dataset. Intended to be used at training time.
        """
        cur_batch = Batch(quote_context_size, speaker_repr_nb, batch_size)

        for chapter in shuffle_(copy.copy(self.chapters)):

            for quote in shuffle_(copy.copy(chapter.quotes)):

                # qote is considered invalid if one of the quote
                # has a bigger size than its supposedly surrounding context
                if quote.context_size() < quote.quote_size():
                    continue

                for speaker_name in quote.document.characters.keys():

                    if len(quote.closest_speaker_mentions(speaker_name, 1)) == 0:
                        continue

                    (
                        quote_context,
                        quote_context_attention_mask,
                        quote_span_coords,
                        speaker_span_coords,
                        is_speaker,
                    ) = quote.prepare(speaker_name, speaker_repr_nb)

                    cur_batch.append_(
                        quote_context,
                        quote_context_attention_mask,
                        quote_span_coords,
                        speaker_span_coords,
                        is_speaker,
                    )

                    if len(cur_batch) == batch_size:
                        yield cur_batch
                        cur_batch = Batch(
                            quote_context_size, speaker_repr_nb, batch_size
                        )

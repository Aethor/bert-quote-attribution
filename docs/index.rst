.. Bert Quote Attribution documentation master file, created by
   sphinx-quickstart on Wed Jul  8 18:22:58 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. role:: bash(code)
   :language: bash

Bert Quote Attribution Documentation
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

* :ref:`genindex`
* :ref:`modindex`

Welcome to the **Bert Quote Attribution Documentation** !

*Please note that the API is undergoing a streamlining process and may be modified*

Train
=====

The train module can be used to train a speaker prediction model.

Command-line usage
------------------

You can train a model using the command line interface. Here is a simple example :

.. code-block:: bash
		
   python3 train.py --training-corpus './datas/emma.xml' --scoring-corpus './datas/steppe.xml' --quote-context-size 512 --speaker-repr-nb 8 --lstm-layers-nb 1 --lstm-hidden-size 128 --bert-model-type 'bert-base-cased' --learning-rate 1e-5 --epochs-nb 5 --batch-size 4 --model-path "./models/my_model"


Use :bash:`python3 train.py --help` for more informations.


Python example usage
--------------------

Here is a minimal training example using python :

.. code-block:: python

   from transformers import AdamW
   
   from datas import Dataset, Document
   from config import get_args, train_arg_definitions
   from model import SpeakerPredictorModel, RawSpeakerPredictorModel, SpeakerPredictorModelConfig
   from train import train_model

   # command line argument loading
   # argument defaults are specified in config.Defaults
   args = get_args(train_args_definitions)

   # datas loading
   train_dataset = Dataset(
	Document.documents_from_descriptions(args.training_corpus, args.quote_context_size)
   )
   score_dataset = Dataset(
	Document.documents_from_descriptions(args.scoring_corpus, args.quote_context_size)
   )

   # model creation
   model_config = SpeakerPredictorModelConfig(
	args.quote_context_size,
	args.speaker_repr_nb,
	args.lstm_layers_nb,
	args.lstm_hidden_size,
	args.bert_model_type
   )
   model = SpeakerPredictorModel(RawSpeakerPredictorModel(model_config), model_config)

   # training
   model = train_model(
	model,
	train_dataset,
	score_dataset,
	AdamW(model.raw_model.parameters(), lr=args.learning_rate),
	args.epochs_nb,
	args.batch_size,
   )

   # saving 
   model.save(args.model_path)


Module reference
----------------

.. automodule:: train
   :members:


Predict
=======

The predict module can be used to annotate a dataset with speaker or addressees label using an already trained model.

Module reference
----------------

.. automodule:: predict
   :members:


Score
=====

The score module can evaluate the performance of a model.

Command-line usage
------------------

Here is a basic example that scores the model saved under "./models/my_model" on "The Steppe" :

.. code-block:: bash

   python3 score.py --input-files "./datas/steppe.xml" --model-path "./models/my_model"


See :bash:`python3 score.py --help` for more informations.


Python example usage
--------------------

.. code-block:: python

   from config import get_args, user_arg_definitions
   from model import SpeakerPredictorModel
   from predict import predict_speakers
   from score import score_speakers


   # loads arguments from the command line
   args = get_args(user_arg_definitions)

   # loads dataset using the provided command line arguments
   score_dataset = Dataset(
	Document.documents_from_descriptions(args.input_files, args.quote_context_size)
   )

   # creates model
   model = SpeakerPredictorModel.load(args.model_path)

   # tags dataset with predictions from the model
   score_dataset = predict_speakers(model, score_dataset, args.threshold)

   # score tagged dataset
   precision, recall, f1 = score_speakers(score_dataset)
   print(f"precision : {precision}")
   print(f"recall : {recall}")
   print(f"f1-score : {f1}")


Module reference
----------------

.. automodule:: score
   :members:


Model      
=====

The model module contains the pytorch model.

Module reference
----------------

.. automodule:: model
   :members:


Datas
=====

The datas module take cares of loading datasets.

Module reference
----------------

.. automodule:: datas
   :members:


Config
======

The Config module is used to parse command line arguments.

Module reference
----------------

.. automodule:: config
   :members:


Static
======

The Static module manages common static members

Module reference
----------------

.. automodule:: static
   :members:


Utils
=====

The utils module contains miscelanous useful functions

Module reference
----------------

.. automodule:: utils
   :members:


Visualize
=========

The visualize module contains an attention visualizer GUI.

Prerequisites
-------------

GTK+ and PyGObject are needed. See https://python-gtk-3-tutorial.readthedocs.io/en/latest/install.html

Command-line example
--------------------

.. code-block:: bash
		
   python3 visualize.py --model-path './models/my-model' --input-file './datas/emma.xml'

See :bash:`python3 visualize.py --help` for more informations.


Module reference
----------------

.. automodule:: visualize
   :members:

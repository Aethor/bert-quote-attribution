from typing import List, Callable, Optional
import json
import re

from datas import Document, Dataset, Quote
from config import (
    get_args,
    user_arg_definitions,
    arg_input_files,
    arg_model_path,
    ArgDefinition,
)
from static import get_tokenizer, get_device
from model import SpeakerPredictorModel
from predict import predict_speakers_raw
from score import score_addressees, score_addressees_exact
from utils import sliding_window


def get_speaker(quote: Quote, use_gold_speaker: bool) -> Optional[str]:
    if quote is None:
        return None
    return quote.speaker if use_gold_speaker else quote.predicted_speaker


def predict_addressees_close(
    dataset: Dataset, use_gold_speaker: bool = False
) -> Dataset:
    for chapter in dataset.chapters:

        for quotes in sliding_window(chapter.quotes, n=3, padding=1):

            cur_quote = quotes[1]
            cur_quote.predicted_addressees = set()

            prev_quote_speaker = get_speaker(quotes[0], use_gold_speaker)
            next_quote_speaker = get_speaker(quotes[2], use_gold_speaker)

            if not prev_quote_speaker is None and prev_quote_speaker != get_speaker(
                cur_quote, use_gold_speaker
            ):
                cur_quote.predicted_addressees.add(prev_quote_speaker)

            if not next_quote_speaker is None and next_quote_speaker != get_speaker(
                cur_quote, use_gold_speaker
            ):
                cur_quote.predicted_addressees.add(next_quote_speaker)

    return dataset


def predict_addressees_far(dataset: Dataset, use_gold_speaker: bool = False) -> Dataset:
    for chapter in dataset.chapters:

        for quotes in sliding_window(chapter.quotes, n=5, padding=2):

            cur_quote = quotes[2]
            cur_quote.predicted_addressees = set(
                [
                    get_speaker(q, use_gold_speaker)
                    for q in quotes
                    if not get_speaker(q, use_gold_speaker) is None
                    and not get_speaker(q, use_gold_speaker)
                    == get_speaker(cur_quote, use_gold_speaker)
                ]
            )

    return dataset


def predict_addressees_strict(
    dataset: Dataset, use_gold_speaker: bool = False
) -> Dataset:
    for chapter in dataset.chapters:

        for quotes in sliding_window(chapter.quotes, n=5, padding=2):

            cur_quote = quotes[2]

            character_count = {
                c: 0 for doc in dataset.docs for c in doc.characters.keys()
            }
            for quote in quotes[:2] + quotes[3:]:
                speaker = get_speaker(quote, use_gold_speaker)
                if speaker is None:
                    continue
                character_count[speaker] += 1

            cur_quote.predicted_addressees = set(
                [c for c in character_count.keys() if character_count[c] > 1]
            )

    return dataset


def predict_addressees_smart(
    dataset: Dataset, use_gold_speaker: bool = False
) -> Dataset:
    for chapter in dataset.chapters:

        for quotes in sliding_window(chapter.quotes, n=5, padding=2):

            cur_quote = quotes[2]

            characters = {
                character: properties["aliases"]
                for doc in dataset.docs
                for character, properties in doc.characters.items()
            }
            characters_count = {c: 0 for c in characters.keys()}
            for character, aliases in characters.items():
                for alias in aliases:
                    matches = re.findall(alias, cur_quote.text)
                    if not matches:
                        continue
                    for match in matches:
                        characters_count[character] += 1
            direct_character = max(characters_count, key=characters_count.get)
            if characters_count[character] > 0:
                cur_quote.predicted_addressees = set([direct_character])
                continue

            cur_quote.predicted_addressees = set(
                [
                    get_speaker(q, use_gold_speaker)
                    for q in quotes
                    if not get_speaker(q, use_gold_speaker) is None
                    and not get_speaker(q, use_gold_speaker)
                    == get_speaker(cur_quote, use_gold_speaker)
                ]
            )

    return dataset


addressees_prediction_functions: List[Callable[[Dataset, bool], Dataset]] = [
    predict_addressees_close,
    predict_addressees_far,
    predict_addressees_strict,
    predict_addressees_smart,
]


arg_output_file = ArgDefinition(
    "-of",
    "--output-file",
    {"default": "addressees_experiments_out.json", "help": "Experiment output file"},
)
arg_model_paths = ArgDefinition(
    "-mp",
    "--model_paths",
    {
        "nargs": "*",
        "default": [
            "./models/spanbert_pp_steppe",
            "./models/spanbert_emma_steppe",
            "./models/spanbert_emma_pp",
        ],
    },
)
arg_input_files.parameters["default"] = [
    "./datas/emma.xml[0:1]",
    "./datas/pp.xml[0:1]",
    "./datas/steppe.xml[0:1]",
]
args = get_args(
    user_arg_definitions.union(set((arg_output_file, arg_model_paths)))
    - set((arg_model_path,))
)

print(f"running with config : {json.dumps(vars(args), indent=4)}")


tokenizer = get_tokenizer(args.bert_tokenizer_type)
device = get_device(args.device)

output_dict = {}

for file_desc, model_path in zip(args.input_files, args.model_paths):

    print(f"scoring {file_desc}")
    output_dict[file_desc] = {}

    dataset = Dataset(
        Document.documents_from_descriptions([file_desc], args.quote_context_size)
    )
    model = SpeakerPredictorModel.load(model_path)

    dataset = predict_speakers_raw(
        model.raw_model, dataset, model.config, args.threshold
    )

    for prediction_fn in addressees_prediction_functions:

        print(f"scoring {prediction_fn.__name__}")
        output_dict[file_desc][prediction_fn.__name__] = {}

        dataset.reset_addressees_predictions()

        print(f"scoring with speaker predictions")
        dataset = prediction_fn(dataset)
        precision, recall, f1 = score_addressees(dataset)
        exact_accuracy = score_addressees_exact(dataset)
        print(f"precision : {precision}")
        print(f"recall : {recall}")
        print(f"f1-score : {f1}")
        print(f"exact accuracy : {exact_accuracy}")
        output_dict[file_desc][prediction_fn.__name__]["prediction labels"] = {
            "precision": precision,
            "recall": recall,
            "f1": f1,
            "exact accuracy": exact_accuracy,
        }

        print(f"scoring with gold labels")
        dataset = prediction_fn(dataset, use_gold_speaker=True)
        precision, recall, f1 = score_addressees(dataset)
        exact_accuracy = score_addressees_exact(dataset)
        print(f"precision : {precision}")
        print(f"recall : {recall}")
        print(f"f1-score : {f1}")
        print(f"exact accuracy : {exact_accuracy}")
        output_dict[file_desc][prediction_fn.__name__]["gold labels"] = {
            "precision": precision,
            "recall": recall,
            "f1": f1,
            "exact accuracy": exact_accuracy,
        }


with open(args.output_file, "w") as f:
    f.write(json.dumps(output_dict, indent=4))
print(f"saved test result in {args.output_file}")

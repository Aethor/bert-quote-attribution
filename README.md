# Welcome to the bert-quote-attribution project

This project aims at automatically predicting quote speakers in literary texts using BERT based models.

# Setup

* `pip install -r requirements.txt` will installed the required python libraries

# Documentation

You can find the documentation of the project at https://aethor.gitlab.io/bert-quote-attribution/

In case you need it, you can also build it locally with sphinx. Provided you installed the dependencies in *requirements.txt* :

```sh
cd docs
make html
```

You can then consult the *docs/_build/html/index.html* file.

# Paper

A (somewhat deprecated) paper was written explaining the concept of the project. It can be found under the *paper* folder. Provided you have a latex distribution, run `make` to generate it.

For a more recent overview, see my [master thesis](https://gitlab.com/Aethor/master-thesis).


# Training

the `train.py` script has the capability to train a model. Use `train.py --help` for all possible options. 


# Attention visualizer

An attention visualizer is included with the `visualize.py` script. to use it, you will also need GTK+3 and PyGObject (https://python-gtk-3-tutorial.readthedocs.io/en/latest/install.html#dependencies). The related python modules are not included in the `requirements.py` file.

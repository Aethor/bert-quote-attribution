from typing import Tuple
import os
import json

import torch
from transformers import BertConfig
from transformers import AutoModel

from static import get_device


class SpeakerPredictorModelConfig:
    """
    Configuration for the model
    """

    def __init__(
        self,
        quote_context_size: int,
        speaker_repr_nb: int,
        lstm_layers_nb: int,
        lstm_hidden_size: int,
        bert_model_type: str,
    ):
        self.quote_context_size = quote_context_size
        self.speaker_repr_nb = speaker_repr_nb
        self.lstm_layers_nb = lstm_layers_nb
        self.lstm_hidden_size = lstm_hidden_size
        self.bert_model_type = bert_model_type


class BertEncoder(torch.nn.Module):
    """
    Bert encoder for a quote and its context
    """

    def __init__(self, bert_model_type: str):
        super(BertEncoder, self).__init__()

        self.config = BertConfig.from_pretrained(
            bert_model_type, output_attentions=True
        )
        self.bert = AutoModel.from_pretrained(bert_model_type, config=self.config)

        self.heads_nb = self.bert.config.num_attention_heads
        self.hidden_size = self.bert.config.hidden_size

    def forward(
        self, quote_context: torch.Tensor, attention_mask: torch.Tensor
    ) -> Tuple[torch.Tensor, torch.Tensor]:
        """
        :param quote_context:  (batch_size, quote_context_size)
        :param attention_mask: (batch_size, quote_context_size) 
        :return: | a tuple of tensors
            | **out**       : (batch_size, quote_context_size, hidden_size)
            | **attention** : (batch_size, heads_nb, quote_context_segment_size, quote_context_segment_size)
        """
        segment, _, attention = self.bert(quote_context, attention_mask=attention_mask)
        return segment, attention


class QuoteEncoder(torch.nn.Module):
    """
    Quote encoder. Encodes a quote using its first and last tokens, and a LSTM followed by mean pooling
    """

    def __init__(self, config: SpeakerPredictorModelConfig):
        super().__init__()

        self.config = config

        self.bert_encoder = BertEncoder(config.bert_model_type)

        self.lstm_encoder = torch.nn.LSTM(
            self.bert_encoder.hidden_size,
            self.config.lstm_hidden_size,
            self.config.lstm_layers_nb,
            bidirectional=True,
        )

    def forward(
        self,
        quote_context: torch.Tensor,
        quote_context_attention_mask: torch.Tensor,
        quote_span_coords: torch.Tensor,
        speaker_span_coords: torch.Tensor,
    ) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor]:
        """
        :param quote_context:                (batch_size, quote_context_size)
        :param quote_context_attention_mask: (batch_size, quote_context_size)
        :param quote_span_coords:            (batch_size, 2)
        :param speaker_span_coords:          (batch_size, speaker_repr_nb, 2)
        :note: (-1, -1) speaker_span_coord will function as padding
        :return: | a tuple 
            | **quote_repr**                    (batch_size, 2 * hidden_size + 2 * lstm_hidden_size)
            | **speakers_repr**                 (batch_size, 2 * hidden_size)
            | **bert_quote_context_attentions** list(segment_nb)(tensors(batch_size, heads_nb, quote_context_size, quote_context_size))
        """
        batch_size = quote_context.shape[0]

        # (batch_size, quote_repr_size, hidden_size)
        bert_quote_context, bert_quote_context_attentions = self.bert_encoder(
            quote_context, quote_context_attention_mask
        )

        # (batch_size, 2, hidden_size)
        bert_bound_quote_span = torch.zeros(
            batch_size, 2, self.bert_encoder.hidden_size
        ).to(bert_quote_context.device)
        for i in range(batch_size):
            bert_bound_quote_span[i][0] = bert_quote_context[i].index_select(
                0, quote_span_coords[i][0]
            )
            bert_bound_quote_span[i][1] = bert_quote_context[i].index_select(
                0, quote_span_coords[i][1]
            )
        # (batch_size, 2 * hidden_size)
        bert_bound_quote_span = torch.flatten(bert_bound_quote_span, start_dim=1)

        # list(batch_size)(tensors(var quote_size, hidden_size))
        quote_spans = []
        for i in range(batch_size):
            quote_spans.append(
                bert_quote_context[i][quote_span_coords[i][0] : quote_span_coords[i][1]]
            )
        # sort by quote_size and keep original indexes
        quote_spans, quote_spans_idx = zip(
            *list(
                reversed(
                    sorted(
                        zip(quote_spans, range(len(quote_spans))),
                        key=lambda e: e[0].shape[0],
                    )
                )
            )
        )
        packed_quote_spans = torch.nn.utils.rnn.pack_sequence(quote_spans)

        # (max_quote_size, batch_size, 2 * lstm_hidden_size)
        encoded_quote_spans = self.lstm_encoder(packed_quote_spans)[0]
        encoded_quote_spans = torch.nn.utils.rnn.pad_packed_sequence(
            encoded_quote_spans
        )[0]
        # unsort the padded sequence to get the original order back
        encoded_quote_spans = torch.stack(
            list(
                zip(
                    *sorted(
                        zip(
                            [encoded_quote_spans[:, i, :] for i in range(batch_size)],
                            range(batch_size),
                        ),
                        key=lambda e: quote_spans_idx[e[1]],
                    )
                )
            )[0],
            dim=1,
        )

        # (batch_size, max_quote_size, 2 * lstm_hidden_size)
        encoded_quote_spans = encoded_quote_spans.transpose(0, 1)
        # (batch_size, 2 * lstm_hidden_size)
        quote_span_weighted_average = torch.mean(encoded_quote_spans, dim=1)

        # (batch_size, 2 * hidden_size + 2 * lstm_hidden_size)
        quote_repr = torch.cat((bert_bound_quote_span, quote_span_weighted_average), 1)

        # (batch_size, speaker_repr_nb, 2, hidden_size)
        speakers_repr = torch.zeros(
            batch_size, self.config.speaker_repr_nb, 2, self.bert_encoder.hidden_size
        ).to(bert_quote_context.device)
        for i in range(batch_size):
            for j in range(self.config.speaker_repr_nb):
                if (
                    speaker_span_coords[i][j][0].item() == -1
                    and speaker_span_coords[i][j][1].item() == -1
                ):
                    continue

                speakers_repr[i][j][0, :] = bert_quote_context[i].index_select(
                    0, speaker_span_coords[i][j][0]
                )
                speakers_repr[i][j][1, :] = bert_quote_context[i].index_select(
                    0, speaker_span_coords[i][j][1]
                )
        speakers_repr = torch.flatten(speakers_repr, start_dim=1)

        return (quote_repr, speakers_repr, bert_quote_context_attentions)


class RawSpeakerPredictorModel(torch.nn.Module):
    """
    Raw model for predicting a speaker
    """

    def __init__(self, config: SpeakerPredictorModelConfig):
        super().__init__()

        self.config = config

        self.quote_encoder = QuoteEncoder(config)

        self.linear = torch.nn.Linear(
            config.speaker_repr_nb * (2 * self.quote_encoder.bert_encoder.hidden_size)
            + 2 * self.quote_encoder.bert_encoder.hidden_size
            + 2 * self.quote_encoder.config.lstm_hidden_size,
            2,
        )

    def forward(
        self,
        quote_context: torch.Tensor,
        quote_context_attention_mask: torch.Tensor,
        quote_span_coords: torch.Tensor,
        speaker_span_coords: torch.Tensor,
    ) -> tuple:
        """
        :param quote_context:                (batch_size, quote_repr_size)
        :param quote_context_attention_mask: (batch_size, quote_repr_size)
        :param quote_span_coords:            (batch_size, 2)
        :param speaker_span_coords:          (batch_size, speaker_repr_nb, 2)
        :return: | a tuple :
            | **out**                           : (batch_size, 2)
            | **bert_quote_context_attentions** : list(segment_nb)(tensors(batch_size, heads_nb, quote_context_size, quote_context_size))
        """
        (quote_repr, speakers_repr, bert_quote_context_attentions) = self.quote_encoder(
            quote_context,
            quote_context_attention_mask,
            quote_span_coords,
            speaker_span_coords,
        )
        # (batch_size, speaker_repr_nb * (2 * hidden_size) + 2 * hidden_size + 2 * lstm_hidden_size)
        linear_in = torch.cat((quote_repr, speakers_repr), 1)

        return (self.linear(linear_in), bert_quote_context_attentions)


class SpeakerPredictorModel:
    """
    Speaker predictor model. Contains a raw model and its config.
    """

    def __init__(
        self, raw_model: RawSpeakerPredictorModel, config: SpeakerPredictorModelConfig
    ):
        self.raw_model = raw_model
        self.config = config

    def save(self, model_path: str):
        """
        Save the model under the specified directory
        """
        if model_path[-1] == "/":
            model_path = model_path[:-1]

        os.makedirs(model_path, exist_ok=True)

        torch.save(self.raw_model.state_dict(), f"{model_path}/model.pth")

        with open(f"{model_path}/config.json", "w") as f:
            json.dump(vars(self.config), f, indent=4)

    @staticmethod
    def load(model_path: str) -> "SpeakerPredictorModel":
        """
        Loads a model from a directory. The directory must contain a 'model.pth' file and a 'config.json' file.
        """
        if model_path[-1] == "/":
            model_path = model_path[:-1]

        with open(f"{model_path}/config.json", "r") as f:
            config = SpeakerPredictorModelConfig(**json.load(f))

        raw_model = RawSpeakerPredictorModel(config)
        raw_model.load_state_dict(
            torch.load(f"{model_path}/model.pth", map_location=get_device()),
            strict=False,
        )

        return SpeakerPredictorModel(raw_model, config)

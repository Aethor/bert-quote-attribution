from typing import List, Any, Generator, Union, Tuple, Iterable, TypeVar, Optional
from itertools import islice
import random

import torch


random.seed(1)


def shuffle_(seq: List[Any]) -> Generator[Any, None, None]:
    """
    Return a generator over the shuffled input sequence

    .. warning::
        This functions destroys the input list. Use a copy as input if needed !
    
    :param seq: the input sequence
    """
    for cur_len in range(len(seq), 0, -1):
        random_i = random.randrange(0, cur_len)
        yield seq[random_i]
        del seq[random_i]


def pad_tensor_1d(
    t: torch.Tensor,
    pad_len: int,
    pad_symbol=0,
    generate_attention_mask: bool = False,
    location: str = "right",
) -> Union[torch.Tensor, Tuple[torch.Tensor, torch.Tensor]]:
    """
    Pad input tensor with pad_symbol
    
    :param t: input tensor
    :param pad_len: max tensor size
    :param pad_symbol: pad symbol, defaults to 0
    :param generate_attention_mask: if true, returns an attention mask
    :param padding_location: wether to pad on the left or on the right.
        only accept string "left" or "right", otherwise default to "right"
    :return: 
        generate_attention_mask is True : padded_tensor (pad_len), attention_mask (pad_len)
        generate_attention_mask is False : padded_tensor (pad_len)
    """
    if location != "left" and location != "right":
        raise Exception(f"incorrect padding location : {location}")

    if t.shape[0] > pad_len:
        raise Exception(
            f"[warning] tensor size {t.shape[0]} is greater than pad len {pad_len}"
        )

    padded_t = torch.tensor([pad_symbol] * pad_len)

    if location == "right":
        padded_t[: t.shape[0]] = t
        if generate_attention_mask:
            attention_mask = torch.zeros(pad_len)
            attention_mask[: t.shape[0]] = torch.ones(t.shape[0])
    elif location == "left":
        padded_t[t.shape[0] :] = t
        if generate_attention_mask:
            attention_mask = torch.zeros(pad_len)
            attention_mask[t.shape[0] :] = torch.ones(t.shape[0])

    if generate_attention_mask:
        return padded_t, attention_mask
    return padded_t


def pad_list(seq: list, pad_len: int, pad_symbol=0, location: str = "right") -> list:
    """
    Pad input sequence with pad_symbol on the left
    
    :param seq: input sequence, will be modified in place
    :param pad_len: max sequence size
    :param pad_symbol: pad symbol, defaults to 0
    :param location: wether to pad on the left or on the right.
        only accept strings "left" or "right"
    :return: newly generated sequence
    """
    if location != "left" and location != "right":
        raise Exception(f"incorrect padding location : {location}")

    if len(seq) > pad_len:
        print(f"[warning] sequence size {len(seq)} was greated than pad len {pad_len}")
        return seq

    padded_seq = [pad_symbol] * pad_len

    if location == "right":
        padded_seq[: len(seq)] = seq
    elif location == "left":
        padded_seq[len(seq) :] = seq

    return padded_seq


T = TypeVar("T")


def sliding_window(
    seq: Iterable[T], n: int = 2, padding: int = 0
) -> Generator[Optional[T], None, None]:
    """
    Sliding window generator over a sequence
    
    :param seq: input sequence
    :param n: sliding window size
    :param padding: sliding window padding size (None will be used to pad)
    """
    if n <= 0:
        raise Exception(f"wrong value of n : {n}")

    iterator = iter(seq)
    window = [None] * padding + list(islice(iterator, n - padding))
    end_padding = padding + len(window) - n
    if len(window) < n:
        window += [None] * (n - len(window))
    if len(window) == n:
        yield window
    for elem in iterator:
        window = window[1:] + [elem]
        yield window
    for _ in range(end_padding):
        window = window[1:] + [None]
        yield window
